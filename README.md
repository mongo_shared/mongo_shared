Cara membuat mongo shared + replicate database

1. Membuat 3 server yang akan di shared, dengan 1 primary yaitu port 27065 dan 2 secondary seperti contoh di bawah ini :

start mongod --shardsvr --dbpath "C:\data\sharedandreplicate01\shared01" --logpath "C:\data\sharedandreplicate01\shared01\log\monod1.log" --port 27065 --replSet RS 
start mongod --shardsvr --dbpath "C:\data\sharedandreplicate01\shared02" --logpath "C:\data\sharedandreplicate01\shared02\log\monod2.log" --port 27066 --replSet RS 
start mongod --shardsvr --dbpath "C:\data\sharedandreplicate01\shared03" --logpath "C:\data\sharedandreplicate01\shared03\log\monod3.log" --port 27067 --replSet RS 


2. Membuat 3 server replicate, dengan 1 primary yaitu port 27068 dan 2 secondary seperti contoh di bawah ini :

start mongod --configsvr --dbpath "C:\data\sharedandreplicate01\replica01" --logpath "C:\data\sharedandreplicate01\replica01\log\mongoc1.log" --port 27068 --replSet 

CRS 
start mongod --configsvr --dbpath "C:\data\sharedandreplicate01\replica02" --logpath "C:\data\sharedandreplicate01\replica02\log\mongoc2.log" --port 27069 --replSet 

CRS 
start mongod --configsvr --dbpath "C:\data\sharedandreplicate01\replica03" --logpath "C:\data\sharedandreplicate01\replica03\log\mongoc3.log" --port 27070 --replSet 

CRS

3. Masuk ke server primary shared yang port 27065

mongo --port 27065, lalu jalankan script di bawah ini

use admin

var config = {_id:'RS',members:
[{_id:0, host:'localhost:27065'},
{_id:1, host:'localhost:27066'},
{_id:2, host:'localhost:27067'}]};
rs.initiate(config)

4. Masuk ke server primary replicate yang port 27068

mongo --port 27068, lalu jalankan script di bawah ini

use admin

var conf={
_id:'CRS',
version:1,
members: [
{_id:0,host:'localhost:27068'},
{_id:1,host:'localhost:27069'},
{_id:2,host:'localhost:27070'}]};
rs.initiate(conf)

5. Membuat route dengan mongos seperti script di bawah ini

mongos --configdb CRS/localhost:27068,localhost:27069,localhost:27070 --port 1005

6. Masuk ke server mongos untuk membuat shared database, shared collection, dan operasi crud dan partisi data seperti script di bawah ini

sh.addShard("RS/localhost:27065,localhost:27066,localhost:27067");

sh.enableSharding("blog")
sh.shardCollection("blog.user",{"userId":1})
sh.splitFind("blog.user",{"userId":1})

use blog

for(var i=100; i < 100; i++){
var user = {
userid:i,
name:'userName'+i,
createAt:new Date().getTime()
}
db.user.save(user);
}


Artikel ini di ambil dari web https://velog.io/@jahommer/mongo-sharding
